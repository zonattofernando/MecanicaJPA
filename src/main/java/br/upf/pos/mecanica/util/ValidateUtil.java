package br.upf.pos.mecanica.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;

public class ValidateUtil {

	/**
	 * Este método recebe uma exceção da persistência e devolve uma List com as Strings de erro. 
	 * @param ex Exceção vinda da persistência.
	 * @return A lista de Strings com as mensagens.
	 */
	public static List<String> exceptionToStringList(Throwable ex){
		List<String> erros = new ArrayList<String>();
		for(ConstraintViolation cv : ((ConstraintViolationException) ex).getConstraintViolations()){
           erros.add(cv.getMessage());			
		}
		return erros;
	}
	
	/**
	 * Este método recebe um objeto qualquer para validar e retorna uma List com Strings de erro.
	 * @param objeto Objeto a ser validado.
	 * @return A lista de Strings com as mensagens.
	 */
	public static List<String> validationToStringList(Object objeto){
		List<String> erros = new ArrayList<String>();
		Set<ConstraintViolation<Object>> violacoes = Validation.buildDefaultValidatorFactory().
				getValidator().validate(objeto);
		for(ConstraintViolation<Object> v : violacoes){
			erros.add(v.getMessage());
		}
		return erros;
	}
	
	
	
	
}
