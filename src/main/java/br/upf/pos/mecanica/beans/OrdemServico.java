package br.upf.pos.mecanica.beans;

import static javax.persistence.GenerationType.SEQUENCE;
import br.upf.pos.mecanica.beans.Cliente;
import br.upf.pos.mecanica.beans.Veiculo;

import java.io.Serializable;
import java.lang.Double;
import java.lang.String;
import java.util.Calendar;
import java.util.List;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: OrdemServico
 *
 */
@Entity
@Table(name = "ordens_servicos")
public class OrdemServico implements Serializable {
	@Version
	private Long versao;
	   
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genOrdemServico")
	@SequenceGenerator(name = "genOrdemServico", sequenceName = "genOrdemServico", schema = "public", allocationSize = 1)
	private Long id;
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	@NotNull(message = "A data de emissão deve ser informada")
	@Past(message = "A data deve ser no passado!")
	private Calendar dataEmissao;
	@Column(nullable = true)
	@Temporal(TemporalType.DATE)
	private Calendar dataConclusao;
	@Column(nullable = false, precision = 2, scale = 10)
	@NotNull(message = "O valor é obrigatório")
	@Min(value = 0, message = "O valor mínimo é de {value}")
	private Double valor;
	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = false, length = 15)
	@NotNull(message = "O status é obrigatório")
	private StatusOrdemServicoEnum status;
	@Lob
	private String observacoes;
	@ManyToOne(optional = false)
	@NotNull(message = "O cliente é obrigatório")
	private Cliente cliente;
	@ManyToOne(optional = false)
	@NotNull(message = "O veículo é obrigatório")
	private Veiculo veiculo;
	@OneToMany(mappedBy = "ordemServico", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@OrderColumn(name = "id")
	@Size(min = 1, message = "Deve ter no mínimo {min} serviço")
	@Valid
	private List<OrdemServicoItem> itens;
	private static final long serialVersionUID = 1L;

	public OrdemServico() {
		super();
	}   
	
	public OrdemServico(Long id, Calendar dataEmissao, Calendar dataConclusao,
			Double valor, StatusOrdemServicoEnum status, String observacoes,
			Cliente cliente, Veiculo veiculo) {
		super();
		this.id = id;
		this.dataEmissao = dataEmissao;
		this.dataConclusao = dataConclusao;
		this.valor = valor;
		this.status = status;
		this.observacoes = observacoes;
		this.cliente = cliente;
		this.veiculo = veiculo;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public Calendar getDataEmissao() {
		return this.dataEmissao;
	}

	public void setDataEmissao(Calendar dataEmissao) {
		this.dataEmissao = dataEmissao;
	}   
	public Calendar getDataConclusao() {
		return this.dataConclusao;
	}

	public void setDataConclusao(Calendar dataConclusao) {
		this.dataConclusao = dataConclusao;
	}   
	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}   
	public StatusOrdemServicoEnum getStatus() {
		return this.status;
	}

	public void setStatus(StatusOrdemServicoEnum status) {
		this.status = status;
	}   
	public String getObservacoes() {
		return this.observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}   
	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}   
	public Veiculo getVeiculo() {
		return this.veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

	@Override
	public String toString() {
		return "OrdemServico: \n" + id + "\n" + dataEmissao
				+ "\n" + dataConclusao + "\n" + valor
				+ "\n" + status + "\n" + observacoes
				+ "\n" + cliente + "\n" + veiculo + "\nItens: \n"
				+ itens;
	}
   
}
