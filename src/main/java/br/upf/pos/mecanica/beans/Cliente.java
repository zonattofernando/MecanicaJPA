package br.upf.pos.mecanica.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

/**
 * Entity implementation class for Entity: Cliente
 *
 */
@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {
	@Version
	private Long versao;
	   
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genCliente")
	@SequenceGenerator(name = "genCliente", sequenceName = "genCliente", schema = "public", allocationSize = 1)
	private Long id;
	@Column(nullable = false, length = 60)
	@NotBlank(message = "O nome é obrigatório")
	@Length(min = 2, max = 60, message = "O nome deve ter entre {min} e {max} caracteres")
	private String nome;
	@Column(nullable = true, length = 14, unique = true)
	private String cpf;
	@Column(nullable = true, length = 18, unique = true)
	private String cnpj;
	@Column(nullable = false, length = 20)
	@NotBlank(message = "O fone é obrigatório")
	@Length(min = 8, max = 20, message = "O fone deve ter entre {min} e {max} caracteres")
	private String fone;
	@Column(nullable = false, length = 25)
	@NotBlank(message = "O e-mail é obrigatório")
	@Length(min = 5, max = 25, message = "O nome deve ter entre {min} e {max} caracteres")
	@Email(message = "O e-mail não é valido")
	private String email;
	@Column(nullable = false, length = 60)
	@NotBlank(message = "A rua é obrigatório")
	@Length(min = 2, max = 25, message = "A rua deve ter entre {min} e {max} caracteres")
	private String rua;
	@Column(nullable = false, length = 25)
	@NotBlank(message = "O número é obrigatório")
	@Length(min = 1, max = 25, message = "O número deve ter entre {min} e {max} caracteres")
	private String numero;
	@Column(nullable = true, length = 40)
	private String complemento;
	@Column(nullable = false, length = 25)
	@NotBlank(message = "O bairro é obrigatório")
	@Length(min = 2, max = 25, message = "O bairro deve ter entre {min} e {max} caracteres")
	private String bairro;
	@Column(nullable = false, length = 60)
	@NotBlank(message = "A cidade é obrigatório")
	@Length(min = 2, max = 60, message = "A cidade deve ter entre {min} e {max} caracteres")
	private String cidade;
	@Column(nullable = false, length = 2)
	@NotBlank(message = "O estado é obrigatório")
	@Length(min = 2, max = 2, message = "O estado deve ter {max} caracteres")
	private String estado;
	@Column(nullable = false, length = 9)
	@NotBlank(message = "O cep é obrigatório")
	@Length(min = 9, max = 9, message = "O cep deve ter {max} caracteres")
	private String cep;
	private static final long serialVersionUID = 1L;

	public Cliente() {
		super();
	}   
	
	public Cliente(Long id, String nome, String cpf, String cnpj, String fone,
			String email, String rua, String numero, String complemento,
			String bairro, String cidade, String estado, String cep) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.cnpj = cnpj;
		this.fone = fone;
		this.email = email;
		this.rua = rua;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.cep = cep;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}   
	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}   
	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}   
	public String getFone() {
		return this.fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getRua() {
		return this.rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}   
	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}   
	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}   
	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}   
	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}   
	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}   
	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

	@Override
	public String toString() {
		return "Cliente [versao=" + versao + ", id=" + id + ", nome=" + nome
				+ ", cpf=" + cpf + ", cnpj=" + cnpj + ", fone=" + fone
				+ ", email=" + email + ", rua=" + rua + ", numero=" + numero
				+ ", complemento=" + complemento + ", bairro=" + bairro
				+ ", cidade=" + cidade + ", estado=" + estado + ", cep=" + cep
				+ "]";
	}
   
}
