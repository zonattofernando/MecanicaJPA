package br.upf.pos.mecanica.beans;

import static javax.persistence.GenerationType.SEQUENCE;
import br.upf.pos.mecanica.beans.OrdemServico;
import br.upf.pos.mecanica.beans.Servico;

import java.io.Serializable;
import java.lang.Double;
import java.lang.Integer;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Entity implementation class for Entity: OrdemServicoItem
 *
 */
@Entity
@Table(name = "ordens_servicos_itens")
public class OrdemServicoItem implements Serializable {
	@Version
	private Long versao;
	   
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genOrdemServicoItem")
	@SequenceGenerator(name = "genOrdemServicoItem", sequenceName = "genOrdemServicoItem", schema = "public", allocationSize = 1)
	private Long id;
	@Column(nullable = false, precision = 2, scale = 10)
	@NotNull(message = "O valor é obrigatório")
	@Min(value = 0, message = "O valor mínimo é de {value}")
	private Double valor;
	@Column(nullable = false)
	@NotNull(message = "A quantidade é obrigatória")
	@Min(value = 1, message = "A quantidade mínima é de {value}")
	private Integer quantidade;
	@Column(nullable = false, precision = 2, scale = 10)
	@NotNull(message = "O total é obrigatório")
	private Double total;
	@ManyToOne(optional = false)
	@NotNull(message = "O serviço é obrigatório")
	private Servico servico;
	@ManyToOne(optional = false)
	@NotNull(message = "A ordem de serviço é obrigatória")
	private OrdemServico ordemServico;
	private static final long serialVersionUID = 1L;

	public OrdemServicoItem() {
		super();
	}   
	
	public OrdemServicoItem(Long id, Double valor, Integer quantidade,
			Double total, Servico servico, OrdemServico ordemServico) {
		super();
		this.id = id;
		this.valor = valor;
		this.quantidade = quantidade;
		this.total = total;
		this.servico = servico;
		this.ordemServico = ordemServico;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}   
	public Integer getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}   
	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}   
	public Servico getServico() {
		return this.servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}   
	public OrdemServico getOrdemServico() {
		return this.ordemServico;
	}

	public void setOrdemServico(OrdemServico ordemServico) {
		this.ordemServico = ordemServico;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

	@Override
	public String toString() {
		return  id + ", " + valor.toString()
				+ ", " + quantidade + ", " + total.toString()
				+ ", " + servico;
	}
   
}
