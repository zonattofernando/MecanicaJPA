package br.upf.pos.mecanica.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.lang.Double;
import java.lang.String;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Servico
 *
 */
@Entity
@Table(name = "servicos")
public class Servico implements Serializable {
	@Version
	private Long versao;
	   
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genServico")
	@SequenceGenerator(name = "genServico", sequenceName = "genServico", schema = "public", allocationSize = 1)
	private Long id;
	@Column(nullable = false, length = 60)
	@NotBlank(message = "O nome é obrigatório")
	@Length(min = 2, max = 60, message = "O nome deve ter entre {min} e {max} caracteres")
	private String nome;
	@Column(nullable = false, precision = 2, scale = 10)
	@NotNull(message = "O valor é obrigatório")
	@Min(value = 0, message = "O valor mínimo é de {value}")
	private Double valor;
	@Lob
	private String descricao;
	private static final long serialVersionUID = 1L;

	public Servico() {
		super();
	}   
	
	public Servico(Long id, String nome, Double valor, String descricao) {
		super();
		this.id = id;
		this.nome = nome;
		this.valor = valor;
		this.descricao = descricao;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}   
	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}   
	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

	@Override
	public String toString() {
		return "Servico [id=" + id + ", nome=" + nome + ", valor=" + valor
				+ ", descricao=" + descricao + "]";
	}
   
}
