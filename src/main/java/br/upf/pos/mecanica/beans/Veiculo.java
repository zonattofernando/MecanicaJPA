package br.upf.pos.mecanica.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Veiculo
 *
 */
@Entity
@Table(name = "veiculos")
public class Veiculo implements Serializable {
	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genVeiculo")
	@SequenceGenerator(name = "genVeiculo", sequenceName = "genVeiculo", schema = "public", allocationSize = 1)
	private Long id;
	@Column(nullable = false, length = 8, unique = true)
	@NotBlank(message = "A placa é obrigatória")
	@Length(min = 8, max = 8, message = "A placa {max} caracteres")
	private String placa;
	@Column(nullable = false)
	@Lob
	@NotBlank(message = "A descrição é obrigatória")
	@Length(min = 2, message = "A descrição deve ter no mínimo {min} caracteres")
	private String descricao;
	private static final long serialVersionUID = 1L;

	public Veiculo() {
		super();
	}   
	
	public Veiculo(Long id, String placa, String descricao) {
		super();
		this.id = id;
		this.placa = placa;
		this.descricao = descricao;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}   
	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

	@Override
	public String toString() {
		return "Veiculo [id=" + id + ", placa=" + placa + ", descricao="
				+ descricao + "]";
	}
   
}
