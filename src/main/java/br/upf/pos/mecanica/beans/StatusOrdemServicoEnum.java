package br.upf.pos.mecanica.beans;

public enum StatusOrdemServicoEnum {
	ABERTA, EM_ANDAMENTO, CONCLUIDA, CANCELADA
}
