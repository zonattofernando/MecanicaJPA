package br.upf.pos.mecanica.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Usuario
 *
 */
@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable {
	@Version
	private Long versao;
	   
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genUsuario")
	@SequenceGenerator(name = "genUsuario", sequenceName = "genUsuario", schema = "public", allocationSize = 1)
	private Long id;
	@Column(nullable = false, length = 60)
	@NotBlank(message = "O nome é obrigatório")
	@Length(min = 2, max = 60, message = "O nome deve ter entre {min} e {max} caracteres")
	private String nome;
	@Column(unique = true, nullable = false, length = 60)
	@NotBlank(message = "O e-mail é obrigatório")
	@Length(min = 2, max = 60, message = "O e-maildeve ter entre {min} e {max} caracteres")
	@Email(message = "O e-mail não é valido")
	private String email;
	@Column(nullable = false, length = 25)
	@NotBlank(message = "A senha é obrigatória")
	@Length(min = 6, max = 25, message = "A senha deve ter entre {min} e {max} caracteres")
	private String senha;
	private static final long serialVersionUID = 1L;

	public Usuario() {
		super();
	}   
	
	public Usuario(Long id, String nome, String email, String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}



	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nome=" + nome + ", email=" + email
				+ ", senha=" + senha + "]";
	}
   
}
