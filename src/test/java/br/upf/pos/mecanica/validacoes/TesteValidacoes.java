package br.upf.pos.mecanica.validacoes;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.upf.pos.mecanica.util.ValidateUtil;
import br.upf.pos.mecanica.beans.Cliente;
import br.upf.pos.mecanica.beans.OrdemServico;

public class TesteValidacoes {
	@Test
	public void validacoesClienteSemNome() {
		Cliente c = new Cliente(null, null, "123.456.789-99", "", "(54) 9999-9999", "teste@@@.com", "Rua teste", "123", "", "Bairro teste", "Passo Fundo", "RS", "99000-000");
		
		List<String> erros = ValidateUtil.validationToStringList(c); 
		System.out.println(erros);
		Assert.assertEquals(1, erros.size() );
		Assert.assertEquals("O nome é obrigatório", erros.get(0)); 
	}
	
	@Test
	public void validacoesOrdemServico() {
		OrdemServico o = new OrdemServico();
		
		List<String> erros = ValidateUtil.validationToStringList(o); 
		System.out.println(erros);
	}
}
