package br.upf.pos.mecanica;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.upf.pos.mecanica.beans.Cliente;
import br.upf.pos.mecanica.dao.GenericDaoImpl;

public class TesteGenericDAOCliente {
	@Test
	public void inserirCliente() throws Exception {
		Cliente c = new Cliente(null, "João Silva", "123.456.789-99", "", "(54) 9999-9999", "teste@aaa.com", "Rua teste", "123", "", "Bairro teste", "Passo Fundo", "RS", "99000-000");
		c = new GenericDaoImpl().gravar(c);
		
		Assert.assertEquals("João Silva", c.getNome());
	}
	
	@Test
	public void atualizarCliente() throws Exception {
		Cliente c = new GenericDaoImpl().getObjetoById(Cliente.class, 5L);
		System.out.println(c);
		c.setNome("João Souza");
		c = new GenericDaoImpl().gravar(c);
		
		Assert.assertEquals("João Souza", c.getNome());
	}	
	
	@Test
	public void listarClientes() {
		List<Cliente> lista = new GenericDaoImpl().getObjetos(Cliente.class);
		
		System.out.println(lista);
		
		Assert.assertEquals(1, lista.size());
	}
	
	@Test
	public void excluirCliente() throws Exception {
		new GenericDaoImpl().excluir(Cliente.class, 5L);
	}
}
