package br.upf.pos.mecanica;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.upf.pos.mecanica.beans.Veiculo;
import br.upf.pos.mecanica.dao.GenericDaoImpl;

public class TesteGenericDAOVeiculo {
	@Test
	public void inserir() throws Exception {
		Veiculo v = new Veiculo(null, "III-0101", "Gol caixa");
		v = new GenericDaoImpl().gravar(v);
		
		Assert.assertEquals("III-0101", v.getPlaca());
	}
	
	@Test
	public void atualizar() throws Exception {
		Veiculo v = new GenericDaoImpl().getObjetoById(Veiculo.class, 1L);
		System.out.println(v);
		v.setPlaca("III-4444");
		v = new GenericDaoImpl().gravar(v);
		
		Assert.assertEquals("III-4444", v.getPlaca());
	}	
	
	@Test
	public void listar() {
		List<Veiculo> lista = new GenericDaoImpl().getObjetos(Veiculo.class);
		
		System.out.println(lista);
		
		Assert.assertEquals(1, lista.size());
	}
	
	@Test
	public void excluir() throws Exception {
		new GenericDaoImpl().excluir(Veiculo.class, 1L);
	}
}