package br.upf.pos.mecanica;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.upf.pos.mecanica.beans.Cliente;
import br.upf.pos.mecanica.beans.OrdemServico;
import br.upf.pos.mecanica.beans.OrdemServicoItem;
import br.upf.pos.mecanica.beans.Servico;
import br.upf.pos.mecanica.beans.StatusOrdemServicoEnum;
import br.upf.pos.mecanica.beans.Veiculo;
import br.upf.pos.mecanica.dao.GenericDaoImpl;

public class TesteGenericDAOOrdemServico {
	@Test
	public void inserir() throws Exception {
		Cliente c = new GenericDaoImpl().getObjetoById(Cliente.class, 7L);
		Veiculo v = new GenericDaoImpl().getObjetoById(Veiculo.class, 2L);
		
		OrdemServico o = new OrdemServico(null, Calendar.getInstance(), null, 500.00, StatusOrdemServicoEnum.ABERTA, "Nova ordem", c, v);
		o = new GenericDaoImpl().gravar(o);
		
		OrdemServicoItem osi1 = new OrdemServicoItem(null, 250.00, 1, 250.00, new GenericDaoImpl().getObjetoById(Servico.class, 1L), o);
		OrdemServicoItem osi2 = new OrdemServicoItem(null, 250.00, 1, 250.00, new GenericDaoImpl().getObjetoById(Servico.class, 2L), o);
		new GenericDaoImpl().gravar(osi1);
		new GenericDaoImpl().gravar(osi2);
		
		Assert.assertEquals("Nova ordem", o.getObservacoes());
	}
	
	@Test
	public void listar() {
		List<OrdemServico> lista = new GenericDaoImpl().getObjetos(OrdemServico.class);
		
		System.out.println(lista);
		
		Assert.assertEquals(1, lista.size());
	}
}