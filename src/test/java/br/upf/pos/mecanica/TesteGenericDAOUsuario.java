package br.upf.pos.mecanica;

import org.junit.Assert;
import org.junit.Test;

import br.upf.pos.mecanica.beans.Usuario;
import br.upf.pos.mecanica.dao.GenericDaoImpl;

public class TesteGenericDAOUsuario {
	@Test
	public void inserirUsuario() throws Exception{
		Usuario u = new Usuario(null, "Fernando", "zonattofernando@gmail.com", "123456");
		u = new GenericDaoImpl().gravar(u);
		
		Assert.assertEquals("Fernando", u.getNome());
	}
}
