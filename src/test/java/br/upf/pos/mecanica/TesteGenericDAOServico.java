package br.upf.pos.mecanica;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.upf.pos.mecanica.beans.Servico;
import br.upf.pos.mecanica.beans.Veiculo;
import br.upf.pos.mecanica.dao.GenericDaoImpl;

public class TesteGenericDAOServico {
	@Test
	public void inserir() throws Exception {
		Servico s = new Servico(null, "Troca de velas", 250.00, "Troca das velas");
		s = new GenericDaoImpl().gravar(s);
		
		Assert.assertEquals("Troca de velas", s.getNome());
	}
	
	@Test
	public void listar() {
		List<Veiculo> lista = new GenericDaoImpl().getObjetos(Veiculo.class);
		
		System.out.println(lista);
		
		Assert.assertEquals(1, lista.size());
	}
}