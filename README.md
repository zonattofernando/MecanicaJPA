Trabalho das disciplinas de Desenvolvimento Ágil Orientado a Testes e Java EE e Persistência de Dados.

O projeto é basicamente um controle de ordens de serviço de uma oficina mecânica.
O sistema possui as seguintes manutenções:
	-Usuários
	-CLientes
	-Veículos
	-Serviços
	-Ordens de Serviços

Os testes estão no pacote de testes e fazem as operações CRUD de todas as classes do projeto. Os testes das ordens de serviços devem ser feitos após o cadastro dos objetos das outras classes.
Os testes DAO utilizam o banco de dados.
Existe um teste de validações também.
